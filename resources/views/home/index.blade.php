
<!DOCTYPE html>
<html lang="en-US"
      xmlns="http://www.w3.org/1999/xhtml"
      itemscope itemtype="http://schema.org/WebPage">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="profile" href="https://gmpg.org/xfn/11">

    <link rel="shortcut icon" href="https://www1.watchasian.to/favicon.png">
    <meta name="google-site-verification" content=""/>

    <title>Watchasian | Asian Drama, Movies and Shows Full HD with english sub</title>

    <meta name="robots" content="index,follow" />
    <meta name="description" content="Asian Drama, Watch drama asian Online for free releases in Korean, Taiwanese, Hong Kong,Thailand and Chinese with English subtitles, Download drama with FullHD">
    <meta name="keywords" content="watchasian , Korean Dramas , Japan Dramas">
    <meta itemprop="image" content="/images/logo.png"/>

    <meta property="og:site_name" content="Watchasian"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="fb:admins" content=""/>
    <meta property="og:title" content="Watchasian | Asian Drama, Movies and Shows Full HD with english sub"/>
    <meta property="og:description" content="Asian Drama, Watch drama asian Online for free releases in Korean, Taiwanese, Hong Kong,Thailand and Chinese with English subtitles, Download drama with FullHD">
    <meta property="og:url" content="https://www1.watchasian.to/"/>
    <meta property="og:image" content="https://www1.watchasian.to/images/logo.png"/>
    <meta property="og:image:secure_url" content="https://www1.watchasian.to/images/logo.png"/>

    <meta property="twitter:card" content="summary"/>
    <meta property="twitter:title" content="Watchasian | Asian Drama, Movies and Shows Full HD with english sub"/>
    <meta property="twitter:description" content="Asian Drama, Watch drama asian Online for free releases in Korean, Taiwanese, Hong Kong,Thailand and Chinese with English subtitles, Download drama with FullHD"/>

    <meta name="csrf-token" content="5e3d75aa72f88">

    <link rel="apple-touch-icon" href="https://www1.watchasian.to/images/icon_58x58.png" />
    <link rel="apple-touch-icon" href="https://www1.watchasian.to/images/icon_192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="https://www1.watchasian.to/images/icon_120x120.png" sizes="120x120" />
    <link rel="apple-touch-icon" href="https://www1.watchasian.to/images/icon_87x87.png" sizes="87x87" />
    <link rel="apple-touch-icon" href="https://www1.watchasian.to/images/icon_80x80.png" sizes="80x80" />
    <link rel="apple-touch-icon" href="https://www1.watchasian.to/images/icon_60x60.png" sizes="60x60" />
    <link rel="apple-touch-icon" href="https://www1.watchasian.to/images/icon_58x58.png" sizes="58x58" />
    <link rel="apple-touch-icon" href="https://www1.watchasian.to/images/icon_32x32.png" sizes="32x32" />
    <link rel="apple-touch-icon" href="https://www1.watchasian.to/images/icon_16x16.png" sizes="16x16" />

    <link rel="canonical" href="https://www1.watchasian.to/"/>
    <link rel="alternate" hreflang="en-us" href="https://www1.watchasian.to/"/>


    <link rel="stylesheet" type="text/css" href="https://www1.watchasian.to/css/font-awesome.min.css?v=3.5.0" />
    <link rel="stylesheet" type="text/css" href="https://www1.watchasian.to/css/main.css?v=3.5.0" />
    <link rel="stylesheet" type="text/css" href="https://www1.watchasian.to/css/mobi.css?v=3.5.0" />
    <link rel="stylesheet" type="text/css" href="https://www1.watchasian.to/css/res.css?v=3.5.0" />

    <link rel="stylesheet" type="text/css" href="https://www1.watchasian.to/plugins/slideshow/css/layerslider.css?v=3.5.0" />

    <script type='text/javascript'>window.__ITGS_started = Date.now();</script>
</head>
<body>
<div class="container">
    <header>
        <ul class="char-list">
            <li><a href="https://www1.watchasian.to/drama-list" title="Drama List">ALL</a></li>
            <li><a href="https://www1.watchasian.to/drama-list/char-start-other.html" title="Drama List With Special Character">#</a></li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-A.html" title="Drama List With A Character">A</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-B.html" title="Drama List With B Character">B</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-C.html" title="Drama List With C Character">C</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-D.html" title="Drama List With D Character">D</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-E.html" title="Drama List With E Character">E</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-F.html" title="Drama List With F Character">F</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-G.html" title="Drama List With G Character">G</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-H.html" title="Drama List With H Character">H</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-I.html" title="Drama List With I Character">I</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-J.html" title="Drama List With J Character">J</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-K.html" title="Drama List With K Character">K</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-L.html" title="Drama List With L Character">L</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-M.html" title="Drama List With M Character">M</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-N.html" title="Drama List With N Character">N</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-O.html" title="Drama List With O Character">O</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-P.html" title="Drama List With P Character">P</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-Q.html" title="Drama List With Q Character">Q</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-R.html" title="Drama List With R Character">R</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-S.html" title="Drama List With S Character">S</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-T.html" title="Drama List With T Character">T</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-U.html" title="Drama List With U Character">U</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-V.html" title="Drama List With V Character">V</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-W.html" title="Drama List With W Character">W</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-X.html" title="Drama List With X Character">X</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-Y.html" title="Drama List With Y Character">Y</a>
            </li>
            <li>
                <a href="https://www1.watchasian.to/drama-list/char-start-Z.html" title="Drama List With Z Character">Z</a>
            </li>
        </ul>

        <div class="logo">
            <a href="https://www1.watchasian.to" class="ads-evt" title="Home">
                <img src="https://www1.watchasian.to/images/logo.jpg" alt="Watchasian - Asian Drama, Movies" />	</a>
            <div class="res_mobi menu_m">
                <div class="left"><a href="#" class="up-down menu_mobile"><img src="/images/mobi/up_down.png" alt="up-dow"></a></div>
                <div class="right"><a href="https://www1.watchasian.to"><img src="/images/mobi/logo.png" alt="logo">
                    </a></div>
            </div>
            <form class="search" action="https://www1.watchasian.to/search">
                <select name="type" id="search-type">
                    <option value="movies">Movies</option>
                    <option value="stars">Stars</option>
                </select>
                <input type="text" id="search-key" name="keyword" placeholder="Search">
                <button><img src="https://www1.watchasian.to/images/button-search.png" alt="button search" /></button>
            </form>
        </div>

        <nav class="menu_top">
            <ul class="navbar">
                <li><a href="https://www1.watchasian.to" title="Home"><img src="https://www1.watchasian.to/images/home.png" alt="home" /></a></li>
                <li>
                    <a href="https://www1.watchasian.to/drama-list" title="Drama List">Drama List</a>
                    <ul class="sub-nav">
                        <li><a href="https://www1.watchasian.to/category/korean-drama" title="Korean Drama">Korean Drama</a></li>
                        <li><a href="https://www1.watchasian.to/category/japanese-drama" title="Japanese Drama">Japanese Drama</a></li>
                        <li><a href="https://www1.watchasian.to/category/taiwanese-drama" title="Taiwanese Drama">Taiwanese Drama</a></li>
                        <li><a href="https://www1.watchasian.to/category/hong-kong-drama" title="Hong Kong Drama">Hong Kong Drama</a></li>
                        <li><a href="https://www1.watchasian.to/category/chinese-drama" title="Chinese Drama">Chinese Drama</a></li>
                        <li><a href="https://www1.watchasian.to/category/american-drama" title="American Drama">American Drama</a></li>
                        <li><a href="https://www1.watchasian.to/category/other-asia-drama" title="Other Asia Drama">Other Asia Drama</a></li>
                        <li><a href="https://www1.watchasian.to/category/thailand-drama" title="Thailand Drama">Thailand Drama</a></li>
                        <li><a href="https://www1.watchasian.to/category/philippine-drama" title="Philippine Drama">Philippine Drama</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Drama Movie</a>
                    <ul class="sub-nav">
                        <li><a href="https://www1.watchasian.to/category/korean-movies" title="Korean Movies">Korean Movies</a></li>
                        <li><a href="https://www1.watchasian.to/category/japanese-movies" title="Japanese Movies">Japanese Movies</a></li>
                        <li><a href="https://www1.watchasian.to/category/taiwanese-movies" title="Taiwanese Movies">Taiwanese Movies</a></li>
                        <li><a href="https://www1.watchasian.to/category/hong-kong-movies" title="Hong Kong Movies">Hong Kong Movies</a></li>
                        <li><a href="https://www1.watchasian.to/category/chinese-movies" title="Chinese Movies">Chinese Movies</a></li>
                        <li><a href="https://www1.watchasian.to/category/american-movies" title="American Movies">American Movies</a></li>
                        <li><a href="https://www1.watchasian.to/category/other-asia-movies" title="Other Asia Movies">Other Asia Movies</a></li>
                        <li><a href="https://www1.watchasian.to/category/thailand-movies" title="Thailand Movies">Thailand Movies</a></li>
                        <li><a href="https://www1.watchasian.to/category/philippine-movies" title="Philippine Movies">Philippine Movies</a></li>
                    </ul>
                </li>
                <li><a href="https://www1.watchasian.to/kshow" title="KShow">KShow</a></li>
                <li><a href="https://www1.watchasian.to/most-popular-drama" title="Popular Drama">Popular Drama</a></li>
                <li><a href="https://www1.watchasian.to/list-star.html" title="Popular Star">Popular Star</a></li>
                <li><a href="https://www1.watchasian.to/calendar" title="Drama Calendar">Drama Calendar</a></li>
                <li><a href="https://dramacool.info/user/request" title="Request Drama">Request Drama</a></li>
                <li class="user">
                    <a href="https://www1.watchasian.to/login.html" title="login dramacool">Login</a>
                </li>
            </ul>
        </nav>
    </header>

    <div class="slide_mobilde">
        <div id="layerslider" class="ls-wp-container">

            <div class="ls-slide" data-ls="slidedelay:3000;transition2d:all;">
                <img src="https://www1.watchasian.to/plugins/slideshow/slides/new/dramacool-the-cursed-2020.jpg" class="ls-bg" alt="The Cursed" title="The Cursed" />      <a href="/drama-detail/the-cursed-2020" class="ls-link"></a>
            </div>

            <div class="ls-slide" data-ls="slidedelay:3000;transition2d:all;">
                <img src="https://www1.watchasian.to/plugins/slideshow/slides/new/dramacool-my-holo-love.jpg" class="ls-bg" alt="My Holo Love" title="My Holo Love" />      <a href="/drama-detail/my-holo-love" class="ls-link"></a>
            </div>

            <div class="ls-slide" data-ls="slidedelay:3000;transition2d:all;">
                <img src="https://www1.watchasian.to/plugins/slideshow/slides/new/dramacool-tel-me-what-you-saw.jpg" class="ls-bg" alt="Tell Me What You Saw" title="Tell Me What You Saw" />      <a href="/drama-detail/tel-me-what-you-saw" class="ls-link"></a>
            </div>

            <div class="ls-slide" data-ls="slidedelay:3000;transition2d:all;">
                <img src="https://www1.watchasian.to/plugins/slideshow/slides/new/dramacool-forest-2020.jpg" class="ls-bg" alt="Forest" title="Forest" />      <a href="/drama-detail/forest-2020" class="ls-link"></a>
            </div>

            <div class="ls-slide" data-ls="slidedelay:3000;transition2d:all;">
                <img src="https://www1.watchasian.to/plugins/slideshow/slides/new/dramacool-itaewon-class.jpg" class="ls-bg" alt="Itaewon Class" title="Itaewon Class" />      <a href="/drama-detail/itaewon-class" class="ls-link"></a>
            </div>

            <div class="ls-slide" data-ls="slidedelay:3000;transition2d:all;">
                <img src="https://www1.watchasian.to/plugins/slideshow/slides/new/dramacool-blue-moon.jpg" class="ls-bg" alt="XX" title="XX" />      <a href="/drama-detail/blue-moon" class="ls-link"></a>
            </div>

            <div class="ls-slide" data-ls="slidedelay:3000;transition2d:all;">
                <img src="https://www1.watchasian.to/plugins/slideshow/slides/new/dramacool-the-game-towards-midnight.jpg" class="ls-bg" alt="The Game Towards Zero" title="The Game Towards Zero" />      <a href="/drama-detail/the-game-towards-midnight" class="ls-link"></a>
            </div>

            <div class="ls-slide" data-ls="slidedelay:3000;transition2d:all;">
                <img src="https://www1.watchasian.to/plugins/slideshow/slides/new/dramacool-romantic-doctor-teacher-kim-2.jpg" class="ls-bg" alt="Romantic Doctor Teacher Kim 2" title="Romantic Doctor Teacher Kim 2" />      <a href="/drama-detail/romantic-doctor-teacher-kim-2" class="ls-link"></a>
            </div>

            <div class="ls-slide" data-ls="slidedelay:3000;transition2d:all;">
                <img src="https://www1.watchasian.to/plugins/slideshow/slides/new/dramacool-money-game.jpg" class="ls-bg" alt="Money Game" title="Money Game" />      <a href="/drama-detail/money-game" class="ls-link"></a>
            </div>

            <div class="ls-slide" data-ls="slidedelay:3000;transition2d:all;">
                <img src="https://www1.watchasian.to/plugins/slideshow/slides/new/dramacool-black-dog.jpg" class="ls-bg" alt="Black Dog" title="Black Dog" />      <a href="/drama-detail/black-dog" class="ls-link"></a>
            </div>

            <div class="ls-slide" data-ls="slidedelay:3000;transition2d:all;">
                <img src="https://www1.watchasian.to/plugins/slideshow/slides/new/dramacool-emergency-lands-of-love.jpg" class="ls-bg" alt="Crash Landing on You" title="Crash Landing on You" />      <a href="/drama-detail/emergency-lands-of-love" class="ls-link"></a>
            </div>

            <div class="ls-slide" data-ls="slidedelay:3000;transition2d:all;">
                <img src="https://www1.watchasian.to/plugins/slideshow/slides/new/dramacool-stove-league.jpg" class="ls-bg" alt="Hot Stove League (2019)" title="Hot Stove League (2019)" />      <a href="/drama-detail/stove-league" class="ls-link"></a>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="content-left">

            <div class="ads_place"></div>

            <div class="block-tab">
                <ul class="tab">
                    <li data-tab="left-tab-1" class="Recently selected">Recently Drama</li>
                    <li class="Added" data-tab="left-tab-2">Recently Movie</li>
                    <li class="Kshow" data-tab="left-tab-3">Recently Kshow</li>
                </ul>
                <ul class="switch-view">
                    <li class="selected" data-view="list-episode-item"></li>
                    <li data-view="list-episode-item-2"></li>
                </ul>
                <div class="block tab-container">
                    <div class="tab-content left-tab-1 selected">
                        <ul class="switch-block list-episode-item">
                            <li>
                                <a href="https://www1.watchasian.to/gracious-revenge-episode-68.html" class="img" title="Gracious Revenge EP 68">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Gracious Revenge" data-original="https://cdn.videokvid.com/cover/elegant-mother-and-daughter.png" />                <span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/gracious-revenge-episode-68.html'">Gracious Revenge</h3>
                                    <span class="time">53 minutes ago</span>
                                    <span class="ep RAW">EP 68</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/down-the-flower-path-episode-74.html" class="img" title="Down the Flower Path EP 74">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Down the Flower Path" data-original="https://cdn.videokvid.com/cover/down-the-flower-path.png" />                <span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/down-the-flower-path-episode-74.html'">Down the Flower Path</h3>
                                    <span class="time">57 minutes ago</span>
                                    <span class="ep RAW">EP 74</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/want-a-taste-episode-64.html" class="img" title="Want a Taste? EP 64">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Want a Taste?" data-original="https://cdn.videokvid.com/cover/wanna-taste.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/want-a-taste-episode-64.html'">Want a Taste?</h3>
                                    <span class="time">2 hours ago</span>
                                    <span class="ep SUB">EP 64</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/bad-love-2019-episode-49.html" class="img" title="Bad Love (2019) EP 49">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Bad Love (2019)" data-original="https://cdn.videokvid.com/cover/bad-love-2019.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/bad-love-2019-episode-49.html'">Bad Love (2019)</h3>
                                    <span class="time">4 hours ago</span>
                                    <span class="ep SUB">EP 49</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/providing-alibi-cracking-episode-1.html" class="img" title="Providing Alibi Cracking EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Providing Alibi Cracking" data-original="https://cdn.videokvid.com/cover/providing-alibi-cracking.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/providing-alibi-cracking-episode-1.html'">Providing Alibi Cracking</h3>
                                    <span class="time">4 hours ago</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/handsome-siblings-2020-episode-30.html" class="img" title="Handsome Siblings (2020) EP 30">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Handsome Siblings (2020)" data-original="https://cdn.videokvid.com/cover/handsome-siblings-2019.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/handsome-siblings-2020-episode-30.html'">Handsome Siblings (2020)</h3>
                                    <span class="time">10 hours ago</span>
                                    <span class="ep SUB">EP 30</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/great-ruler-episode-14.html" class="img" title="Great Ruler EP 14">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Great Ruler" data-original="https://cdn.videokvid.com/cover/great-ruler.png" />                <span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/great-ruler-episode-14.html'">Great Ruler</h3>
                                    <span class="time">11 hours ago</span>
                                    <span class="ep RAW">EP 14</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/royal-nirvana-episode-59.html" class="img" title="Royal Nirvana EP 59">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Royal Nirvana" data-original="https://cdn.videokvid.com/cover/royal-nirvana.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/royal-nirvana-episode-59.html'">Royal Nirvana</h3>
                                    <span class="time">13 hours ago</span>
                                    <span class="ep SUB">EP 59</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/ieyasu-edo-wo-tateru-episode-2.html" class="img" title="Ieyasu, Edo wo Tateru EP 2">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Ieyasu, Edo wo Tateru" data-original="https://cdn.videokvid.com/cover/ieyasu-edo-wo-tateru.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/ieyasu-edo-wo-tateru-episode-2.html'">Ieyasu, Edo wo Tateru</h3>
                                    <span class="time">14 hours ago</span>
                                    <span class="ep SUB">EP 2</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/forest-2020-episode-6.html" class="img" title="Forest (2020) EP 6">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Forest (2020)" data-original="https://cdn.videokvid.com/cover/forest-2020.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/forest-2020-episode-6.html'">Forest (2020)</h3>
                                    <span class="time">14 hours ago</span>
                                    <span class="ep SUB">EP 6</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/great-ruler-episode-4.html" class="img" title="Great Ruler EP 4">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Great Ruler" data-original="https://cdn.videokvid.com/cover/great-ruler.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/great-ruler-episode-4.html'">Great Ruler</h3>
                                    <span class="time">14 hours ago</span>
                                    <span class="ep SUB">EP 4</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/the-game-towards-zero-episode-6.html" class="img" title="The Game: Towards Zero EP 6">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="The Game: Towards Zero" data-original="https://cdn.videokvid.com/cover/the-game-towards-midnight.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/the-game-towards-zero-episode-6.html'">The Game: Towards Zero</h3>
                                    <span class="time">16 hours ago</span>
                                    <span class="ep SUB">EP 6</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/money-game-episode-8.html" class="img" title="Money Game EP 8">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Money Game" data-original="https://cdn.videokvid.com/cover/money-game.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/money-game-episode-8.html'">Money Game</h3>
                                    <span class="time">17 hours ago</span>
                                    <span class="ep SUB">EP 8</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/gracious-revenge-episode-67.html" class="img" title="Gracious Revenge EP 67">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Gracious Revenge" data-original="https://cdn.videokvid.com/cover/elegant-mother-and-daughter.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/gracious-revenge-episode-67.html'">Gracious Revenge</h3>
                                    <span class="time">18 hours ago</span>
                                    <span class="ep SUB">EP 67</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/down-the-flower-path-episode-73.html" class="img" title="Down the Flower Path EP 73">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Down the Flower Path" data-original="https://cdn.videokvid.com/cover/down-the-flower-path.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/down-the-flower-path-episode-73.html'">Down the Flower Path</h3>
                                    <span class="time">18 hours ago</span>
                                    <span class="ep SUB">EP 73</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/forest-2020-episode-8.html" class="img" title="Forest (2020) EP 8">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Forest (2020)" data-original="https://cdn.videokvid.com/cover/forest-2020.png" />                <span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/forest-2020-episode-8.html'">Forest (2020)</h3>
                                    <span class="time">22 hours ago</span>
                                    <span class="ep RAW">EP 8</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/answer-for-heaven-episode-18.html" class="img" title="Answer for Heaven EP 18">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Answer for Heaven" data-original="https://cdn.videokvid.com/cover/answer-for-heaven.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/answer-for-heaven-episode-18.html'">Answer for Heaven</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep SUB">EP 18</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/kamen-rider-black-rx-episode-47.html" class="img" title="Kamen Rider Black RX EP 47">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Kamen Rider Black RX" data-original="https://cdn.videokvid.com/cover/kamen-rider-black-rx.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/kamen-rider-black-rx-episode-47.html'">Kamen Rider Black RX</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep SUB">EP 47</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/dr-cutie-episode-23.html" class="img" title="Dr. Cutie EP 23">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Dr. Cutie" data-original="https://cdn.videokvid.com/cover/dr-cutie.png" />                <span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/dr-cutie-episode-23.html'">Dr. Cutie</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep RAW">EP 23</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/handsome-siblings-2020-episode-33.html" class="img" title="Handsome Siblings (2020) EP 33">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Handsome Siblings (2020)" data-original="https://cdn.videokvid.com/cover/handsome-siblings-2019.png" />                <span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/handsome-siblings-2020-episode-33.html'">Handsome Siblings (2020)</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep RAW">EP 33</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/the-untamed-special-edition-episode-19.html" class="img" title="The Untamed Special Edition EP 19">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="The Untamed Special Edition" data-original="https://cdn.videokvid.com/cover/the-untamed-special-edition.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/the-untamed-special-edition-episode-19.html'">The Untamed Special Edition</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep SUB">EP 19</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/ever-night-season-2-episode-24.html" class="img" title="Ever Night: Season 2 EP 24">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Ever Night: Season 2" data-original="https://cdn.videokvid.com/cover/ever-night-season-2.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/ever-night-season-2-episode-24.html'">Ever Night: Season 2</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep SUB">EP 24</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/fairyland-lovers-episode-30.html" class="img" title="Fairyland Lovers EP 30">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Fairyland Lovers" data-original="https://cdn.videokvid.com/cover/fairyland-lovers.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/fairyland-lovers-episode-30.html'">Fairyland Lovers</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep SUB">EP 30</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/under-the-power-episode-48.html" class="img" title="Under the Power EP 48">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Under the Power" data-original="https://cdn.videokvid.com/cover/under-the-power.png" />                <span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/under-the-power-episode-48.html'">Under the Power</h3>
                                    <span class="time">2 days ago</span>
                                    <span class="ep RAW">EP 48</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/begonia-rouge-episode-52.html" class="img" title="Begonia Rouge EP 52">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Begonia Rouge" data-original="https://cdn.videokvid.com/cover/hai-tang-s-rouge-shines-through-in-the-rain.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/begonia-rouge-episode-52.html'">Begonia Rouge</h3>
                                    <span class="time">2 days ago</span>
                                    <span class="ep SUB">EP 52</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/please-love-me-episode-20.html" class="img" title="Please Love Me EP 20">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Please Love Me" data-original="https://cdn.videokvid.com/cover/please-love-me.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/please-love-me-episode-20.html'">Please Love Me</h3>
                                    <span class="time">2 days ago</span>
                                    <span class="ep SUB">EP 20</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/diary-of-a-prosecutor-episode-14.html" class="img" title="Diary of a Prosecutor EP 14">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Diary of a Prosecutor" data-original="https://cdn.videokvid.com/cover/war-of-prosecutors.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/diary-of-a-prosecutor-episode-14.html'">Diary of a Prosecutor</h3>
                                    <span class="time">2 days ago</span>
                                    <span class="ep SUB">EP 14</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/romantic-doctor-teacher-kim-2-episode-10.html" class="img" title="Romantic Doctor, Teacher Kim 2 EP 10">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Romantic Doctor, Teacher Kim 2" data-original="https://cdn.videokvid.com/cover/romantic-doctor-teacher-kim-2.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/romantic-doctor-teacher-kim-2-episode-10.html'">Romantic Doctor, Teacher Kim 2</h3>
                                    <span class="time">2 days ago</span>
                                    <span class="ep SUB">EP 10</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/black-dog-episode-16.html" class="img" title="Black Dog EP 16">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Black Dog" data-original="https://cdn.videokvid.com/cover/black-dog.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/black-dog-episode-16.html'">Black Dog</h3>
                                    <span class="time">2 days ago</span>
                                    <span class="ep SUB">EP 16</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/under-the-power-episode-36.html" class="img" title="Under the Power EP 36">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Under the Power" data-original="https://cdn.videokvid.com/cover/under-the-power.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/under-the-power-episode-36.html'">Under the Power</h3>
                                    <span class="time">2 days ago</span>
                                    <span class="ep SUB">EP 36</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/we-fall-in-love-2016-episode-20.html" class="img" title="We Fall in Love (2016) EP 20">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="We Fall in Love (2016)" data-original="https://cdn.videokvid.com/cover/we-fall-in-love-2016.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/we-fall-in-love-2016-episode-20.html'">We Fall in Love (2016)</h3>
                                    <span class="time">2020-02-03 23:34:17</span>
                                    <span class="ep SUB">EP 20</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/we-fall-in-love-2016-episode-18.html" class="img" title="We Fall in Love (2016) EP 18">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="We Fall in Love (2016)" data-original="https://cdn.videokvid.com/cover/we-fall-in-love-2016.png" />                <span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/we-fall-in-love-2016-episode-18.html'">We Fall in Love (2016)</h3>
                                    <span class="time">2020-02-03 23:33:51</span>
                                    <span class="ep RAW">EP 18</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/tharntype-the-series-episode-12-5.html" class="img" title="TharnType: The Series EP 12.5">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="TharnType: The Series" data-original="https://cdn.videokvid.com/cover/tharntype-the-series.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/tharntype-the-series-episode-12-5.html'">TharnType: The Series</h3>
                                    <span class="time">2020-02-03 17:43:15</span>
                                    <span class="ep SUB">EP 12.5</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/investigation-meeting-in-the-living-room-sousa-kaigi-wa-living-de-episode-1.html" class="img" title="Investigation Meeting in the Living Room! (Sousa Kaigi wa Living de!) EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Investigation Meeting in the Living Room! (Sousa Kaigi wa Living de!)" data-original="https://cdn.videokvid.com/cover/invistigation-meeting-in-the-living-room-sousa-kaigi-wa-living-de.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/investigation-meeting-in-the-living-room-sousa-kaigi-wa-living-de-episode-1.html'">Investigation Meeting in the Living Room! (Sousa Kaigi wa Living de!)</h3>
                                    <span class="time">2020-02-03 17:02:22</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/follow-saretara-owari-episode-16.html" class="img" title="Follow Saretara Owari EP 16">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Follow Saretara Owari" data-original="https://cdn.videokvid.com/cover/follow-saretara-owari.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/follow-saretara-owari-episode-16.html'">Follow Saretara Owari</h3>
                                    <span class="time">2020-02-03 17:02:02</span>
                                    <span class="ep SUB">EP 16</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/marigold-in-4-minutes-4-punkan-no-marigold-episode-5.html" class="img" title="Marigold in 4 minutes (4-punkan no Marigold) EP 5">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Marigold in 4 minutes (4-punkan no Marigold)" data-original="https://cdn.videokvid.com/cover/marigold-in-4-minutes-4-punkan-no-marigold.png" />                <span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/marigold-in-4-minutes-4-punkan-no-marigold-episode-5.html'">Marigold in 4 minutes (4-punkan no Marigold)</h3>
                                    <span class="time">2020-02-03 17:01:36</span>
                                    <span class="ep SUB">EP 5</span>
                                </a>
                            </li>
                        </ul>
                        <div class="view-more"><a href="https://www1.watchasian.to/recently-added" title="Recently Added Drama">View more</a></div>
                    </div>

                    <div class="tab-content left-tab-2">
                        <ul class="switch-block list-episode-item">
                            <li>
                                <a href="https://www1.watchasian.to/avane-srimannarayana-episode-1.html" class="img" title="Avane Srimannarayana EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Avane Srimannarayana" data-original="https://cdn.videokvid.com/cover/avane-srimannarayana.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/avane-srimannarayana-episode-1.html'">Avane Srimannarayana</h3>
                                    <span class="time">58 minutes ago</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/the-sun-jp-2016-episode-1.html" class="img" title="The Sun (JP 2016) EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="The Sun (JP 2016)" data-original="https://cdn.videokvid.com/cover/the-sun-jp-2016.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/the-sun-jp-2016-episode-1.html'">The Sun (JP 2016)</h3>
                                    <span class="time">12 hours ago</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/metamorphosis-episode-1.html" class="img" title="Metamorphosis EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Metamorphosis" data-original="https://cdn.videokvid.com/cover/metamorphosis.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/metamorphosis-episode-1.html'">Metamorphosis</h3>
                                    <span class="time">2020-02-03 05:12:33</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/agent-phantom-episode-1.html" class="img" title="Agent Phantom EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Agent Phantom" data-original="https://cdn.videokvid.com/cover/agent-phantom.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/agent-phantom-episode-1.html'">Agent Phantom</h3>
                                    <span class="time">2020-02-01 05:24:44</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/timeless-romance-episode-1.html" class="img" title="Timeless Romance EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Timeless Romance" data-original="https://cdn.videokvid.com/cover/timeless-romance.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/timeless-romance-episode-1.html'">Timeless Romance</h3>
                                    <span class="time">2020-02-01 05:24:26</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/fagara-episode-1.html" class="img" title="Fagara EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Fagara" data-original="https://cdn.videokvid.com/cover/fagara.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/fagara-episode-1.html'">Fagara</h3>
                                    <span class="time">2020-01-31 21:28:12</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/mathu-vadalara-episode-1.html" class="img" title="Mathu Vadalara EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Mathu Vadalara" data-original="https://cdn.videokvid.com/cover/mathu-vadalara.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/mathu-vadalara-episode-1.html'">Mathu Vadalara</h3>
                                    <span class="time">2020-01-31 16:12:54</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/37-seconds-episode-1.html" class="img" title="37 Seconds EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="37 Seconds" data-original="https://cdn.videokvid.com/cover/37-seconds.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/37-seconds-episode-1.html'">37 Seconds</h3>
                                    <span class="time">2020-01-31 16:12:40</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/laal-kaptaan-episode-1.html" class="img" title="Laal Kaptaan EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Laal Kaptaan" data-original="https://cdn.videokvid.com/cover/laal-kaptaan.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/laal-kaptaan-episode-1.html'">Laal Kaptaan</h3>
                                    <span class="time">2020-01-31 06:45:08</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/last-love-first-love-cn-2004-episode-1.html" class="img" title="Last Love First Love (CN 2004) EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Last Love First Love (CN 2004)" data-original="https://cdn.videokvid.com/cover/last-love-first-love-cn-2004.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/last-love-first-love-cn-2004-episode-1.html'">Last Love First Love (CN 2004)</h3>
                                    <span class="time">2020-01-31 03:52:56</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/kishiryu-sentai-ryusoulger-the-movie-time-slip-dinosaur-panic-episode-1.html" class="img" title="Kishiryu Sentai Ryusoulger The Movie: Time Slip! Dinosaur Panic!! EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Kishiryu Sentai Ryusoulger The Movie: Time Slip! Dinosaur Panic!!" data-original="https://cdn.videokvid.com/cover/kishiryu-sentai-ryusoulger-the-movie-time-slip-dinosaur-panic.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/kishiryu-sentai-ryusoulger-the-movie-time-slip-dinosaur-panic-episode-1.html'">Kishiryu Sentai Ryusoulger The Movie: Time Slip! Dinosaur Panic!!</h3>
                                    <span class="time">2020-01-29 00:24:58</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/forbidden-dream-episode-1.html" class="img" title="Forbidden Dream EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Forbidden Dream" data-original="https://cdn.videokvid.com/cover/forbidden-dream.png" />					<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/forbidden-dream-episode-1.html'">Forbidden Dream</h3>
                                    <span class="time">2020-01-28 02:08:49</span>
                                    <span class="ep RAW">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/lost-in-russia-episode-1.html" class="img" title="Lost in Russia EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Lost in Russia" data-original="https://cdn.videokvid.com/cover/lost-in-russia.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/lost-in-russia-episode-1.html'">Lost in Russia</h3>
                                    <span class="time">2020-01-27 00:00:45</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/bikers-kental-2-episode-1.html" class="img" title="Bikers Kental 2 EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Bikers Kental 2" data-original="https://cdn.videokvid.com/cover/bikers-kental-2.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/bikers-kental-2-episode-1.html'">Bikers Kental 2</h3>
                                    <span class="time">2020-01-25 20:42:34</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/a-sun-episode-1.html" class="img" title="A Sun EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="A Sun" data-original="https://cdn.videokvid.com/cover/a-sun.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/a-sun-episode-1.html'">A Sun</h3>
                                    <span class="time">2020-01-24 21:20:44</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/single-lady-thai-2015-episode-1.html" class="img" title="Single Lady (Thai 2015) EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Single Lady (Thai 2015)" data-original="https://cdn.videokvid.com/cover/single-lady-thai-2015.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/single-lady-thai-2015-episode-1.html'">Single Lady (Thai 2015)</h3>
                                    <span class="time">2020-01-24 02:28:35</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/ashfall-episode-1.html" class="img" title="Ashfall EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Ashfall" data-original="https://cdn.videokvid.com/cover/ashfall.png" />					<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/ashfall-episode-1.html'">Ashfall</h3>
                                    <span class="time">2020-01-23 00:09:02</span>
                                    <span class="ep RAW">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/run-t-school-basket-club-episode-1.html" class="img" title="Run! T School Basket Club EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Run! T School Basket Club" data-original="https://cdn.videokvid.com/cover/run-t-school-basket-club.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/run-t-school-basket-club-episode-1.html'">Run! T School Basket Club</h3>
                                    <span class="time">2020-01-22 18:01:13</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/kamen-rider-zi-o-over-quartzers-episode-1.html" class="img" title="Kamen Rider Zi-O: Over Quartzers EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Kamen Rider Zi-O: Over Quartzers" data-original="https://cdn.videokvid.com/cover/kamen-rider-zi-o-over-quartzers.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/kamen-rider-zi-o-over-quartzers-episode-1.html'">Kamen Rider Zi-O: Over Quartzers</h3>
                                    <span class="time">2020-01-22 01:35:56</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/hold-on-baseball-team-episode-1.html" class="img" title="Hold On, Baseball Team EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Hold On, Baseball Team" data-original="https://cdn.videokvid.com/cover/hold-on-basball-team.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/hold-on-baseball-team-episode-1.html'">Hold On, Baseball Team</h3>
                                    <span class="time">2020-01-21 17:05:23</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/parallel-world-love-story-episode-1.html" class="img" title="Parallel World Love Story EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Parallel World Love Story" data-original="https://cdn.videokvid.com/cover/parallel-world-love-story.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/parallel-world-love-story-episode-1.html'">Parallel World Love Story</h3>
                                    <span class="time">2020-01-20 23:12:00</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/sultan-agung-tahta-perjuangan-cinta-episode-1.html" class="img" title="Sultan Agung Tahta Perjuangan Cinta EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Sultan Agung Tahta Perjuangan Cinta" data-original="https://cdn.videokvid.com/cover/sultan-agung-tahta-perjuangan-cinta.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/sultan-agung-tahta-perjuangan-cinta-episode-1.html'">Sultan Agung Tahta Perjuangan Cinta</h3>
                                    <span class="time">2020-01-19 16:22:48</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/rasuk-episode-1.html" class="img" title="Rasuk EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Rasuk" data-original="https://cdn.videokvid.com/cover/rasuk.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/rasuk-episode-1.html'">Rasuk</h3>
                                    <span class="time">2020-01-19 16:22:24</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/mega-crocodile-episode-1.html" class="img" title="Mega Crocodile EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Mega Crocodile" data-original="https://cdn.videokvid.com/cover/mega-crocodile.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/mega-crocodile-episode-1.html'">Mega Crocodile</h3>
                                    <span class="time">2020-01-19 06:44:32</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/prince-of-legend-episode-1.html" class="img" title="Prince of Legend EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Prince of Legend" data-original="https://cdn.videokvid.com/cover/prince-of-legend.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/prince-of-legend-episode-1.html'">Prince of Legend</h3>
                                    <span class="time">2020-01-19 05:29:26</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/dhadak-episode-1.html" class="img" title="Dhadak EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Dhadak" data-original="https://cdn.videokvid.com/cover/dhadak.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/dhadak-episode-1.html'">Dhadak</h3>
                                    <span class="time">2020-01-18 08:44:52</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/detention-episode-1.html" class="img" title="Detention EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Detention" data-original="https://cdn.videokvid.com/cover/detention.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/detention-episode-1.html'">Detention</h3>
                                    <span class="time">2020-01-17 02:17:33</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/mori-the-artists-habitat-episode-1.html" class="img" title="Mori, The Artist's Habitat EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Mori, The Artist&amp;#039;s Habitat" data-original="https://cdn.videokvid.com/cover/mori-the-artists-habitat.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/mori-the-artists-habitat-episode-1.html'">Mori, The Artist's Habitat</h3>
                                    <span class="time">2020-01-16 06:24:05</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/it-comes-episode-1.html" class="img" title="It Comes EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="It Comes" data-original="https://cdn.videokvid.com/cover/it-comes.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/it-comes-episode-1.html'">It Comes</h3>
                                    <span class="time">2020-01-15 20:16:28</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/kim-ji-young-born-1982-episode-1.html" class="img" title="Kim Ji Young: Born 1982 EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Kim Ji Young: Born 1982" data-original="https://cdn.videokvid.com/cover/kim-ji-young-born-1982.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/kim-ji-young-born-1982-episode-1.html'">Kim Ji Young: Born 1982</h3>
                                    <span class="time">2020-01-14 16:26:09</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/crazy-romance-episode-1.html" class="img" title="Crazy Romance EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Crazy Romance" data-original="https://cdn.videokvid.com/cover/crazy-romance.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/crazy-romance-episode-1.html'">Crazy Romance</h3>
                                    <span class="time">2020-01-14 16:17:11</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/app-war-episode-1.html" class="img" title="App War EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="App War" data-original="https://cdn.videokvid.com/cover/app-war.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/app-war-episode-1.html'">App War</h3>
                                    <span class="time">2020-01-14 16:15:38</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/recall-episode-1.html" class="img" title="Recall EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Recall" data-original="https://cdn.videokvid.com/cover/recall.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/recall-episode-1.html'">Recall</h3>
                                    <span class="time">2020-01-13 22:53:56</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/detection-of-di-renjie-episode-1.html" class="img" title="Detection Of Di Renjie EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Detection Of Di Renjie" data-original="https://cdn.videokvid.com/cover/detection-of-di-renjie.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/detection-of-di-renjie-episode-1.html'">Detection Of Di Renjie</h3>
                                    <span class="time">2020-01-13 06:42:43</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/the-house-where-the-mermaid-sleeps-episode-1.html" class="img" title="The House Where The Mermaid Sleeps EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="The House Where The Mermaid Sleeps" data-original="https://cdn.videokvid.com/cover/the-house-where-the-the-mermaid-sleeps.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/the-house-where-the-mermaid-sleeps-episode-1.html'">The House Where The Mermaid Sleeps</h3>
                                    <span class="time">2020-01-13 05:29:01</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/dawn-wind-in-my-poncho-episode-1.html" class="img" title="Dawn Wind in My Poncho EP 1">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Dawn Wind in My Poncho" data-original="https://cdn.videokvid.com/cover/dawn-wind-in-my-poncho.png" />					<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/dawn-wind-in-my-poncho-episode-1.html'">Dawn Wind in My Poncho</h3>
                                    <span class="time">2020-01-08 17:43:34</span>
                                    <span class="ep SUB">EP 1</span>
                                </a>
                            </li>
                        </ul>
                        <div class="view-more"><a href="https://www1.watchasian.to/recently-added-movie" title="Recently Added Movie">View more</a></div>
                    </div>

                    <div class="tab-content left-tab-3">
                        <ul class="switch-block list-episode-item">
                            <li>
                                <a href="https://www1.watchasian.to/baek-jong-won-top-3-chef-king-episode-220.html" class="img" title="Baek Jong Won Top 3 Chef King EP 220">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Baek Jong Won Top 3 Chef King" data-original="https://cdn.videokvid.com/20160312033013-baek-jong-won-top-3-chef-king.jpg" />				<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/baek-jong-won-top-3-chef-king-episode-220.html'">Baek Jong Won Top 3 Chef King</h3>
                                    <span class="time">1 hour ago</span>
                                    <span class="ep SUB">EP 220</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/welcome-first-time-in-korea-season-2-episode-92.html" class="img" title="Welcome First Time in Korea Season 2 EP 92">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Welcome First Time in Korea Season 2" data-original="https://cdn.videokvid.com/cover/welcome-first-time-in-korea-season-2.png" />				<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/welcome-first-time-in-korea-season-2-episode-92.html'">Welcome First Time in Korea Season 2</h3>
                                    <span class="time">1 hour ago</span>
                                    <span class="ep SUB">EP 92</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/new-late-night-e-news-episode-144.html" class="img" title="New Late Night E News EP 144">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="New Late Night E News" data-original="https://cdn.videokvid.com/cover/new-late-night-e-news.png" />				<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/new-late-night-e-news-episode-144.html'">New Late Night E News</h3>
                                    <span class="time">12 hours ago</span>
                                    <span class="ep SUB">EP 144</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/lets-eat-dinner-together-episode-161.html" class="img" title="Let's Eat Dinner Together EP 161">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Let&amp;#039;s Eat Dinner Together" data-original="https://cdn.videokvid.com/cover/lets-eat-dinner-together.png" />				<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/lets-eat-dinner-together-episode-161.html'">Let's Eat Dinner Together</h3>
                                    <span class="time">12 hours ago</span>
                                    <span class="ep SUB">EP 161</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/weekly-idol-episode-445.html" class="img" title="Weekly idol EP 445">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Weekly idol" data-original="https://cdn.videokvid.com/20141215020450-weekly idol.jpg" />				<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/weekly-idol-episode-445.html'">Weekly idol</h3>
                                    <span class="time">12 hours ago</span>
                                    <span class="ep SUB">EP 445</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/radio-star-episode-660.html" class="img" title="Radio Star EP 660">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Radio Star" data-original="https://cdn.videokvid.com/images/Radio Star.jpg" />				<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/radio-star-episode-660.html'">Radio Star</h3>
                                    <span class="time">12 hours ago</span>
                                    <span class="ep SUB">EP 660</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/problem-child-in-house-episode-63.html" class="img" title="Problem Child in House EP 63">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Problem Child in House" data-original="https://cdn.videokvid.com/cover/problem-child-in-house.png" />				<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/problem-child-in-house-episode-63.html'">Problem Child in House</h3>
                                    <span class="time">12 hours ago</span>
                                    <span class="ep SUB">EP 63</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/baek-jong-wons-food-alley-episode-103.html" class="img" title="Baek Jong-won's Food Alley EP 103">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Baek Jong-won&amp;#039;s Food Alley" data-original="https://cdn.videokvid.com/cover/baek-jong-wons-food-alley.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/baek-jong-wons-food-alley-episode-103.html'">Baek Jong-won's Food Alley</h3>
                                    <span class="time">13 hours ago</span>
                                    <span class="ep RAW">EP 103</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/can-we-love-again-episode-12.html" class="img" title="Can We Love Again EP 12">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Can We Love Again" data-original="https://cdn.videokvid.com/cover/can-we-love-again.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/can-we-love-again-episode-12.html'">Can We Love Again</h3>
                                    <span class="time">21 hours ago</span>
                                    <span class="ep RAW">EP 12</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/city-fishers-2-episode-7.html" class="img" title="City Fishers 2 EP 7">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="City Fishers 2" data-original="https://cdn.videokvid.com/cover/city-fishers-2.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/city-fishers-2-episode-7.html'">City Fishers 2</h3>
                                    <span class="time">21 hours ago</span>
                                    <span class="ep RAW">EP 7</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/entertainment-news-tonight-episode-143.html" class="img" title="Entertainment News Tonight EP 143">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Entertainment News Tonight" data-original="https://cdn.videokvid.com/cover/entertainment-news-tonight.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/entertainment-news-tonight-episode-143.html'">Entertainment News Tonight</h3>
                                    <span class="time">21 hours ago</span>
                                    <span class="ep RAW">EP 143</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/foreigner-episode-68.html" class="img" title="Foreigner EP 68">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Foreigner" data-original="https://cdn.videokvid.com/cover/foreigner.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/foreigner-episode-68.html'">Foreigner</h3>
                                    <span class="time">22 hours ago</span>
                                    <span class="ep RAW">EP 68</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/fun-restaurant-episode-14.html" class="img" title="Fun Restaurant EP 14">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Fun Restaurant" data-original="https://cdn.videokvid.com/cover/fun-restaurant.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/fun-restaurant-episode-14.html'">Fun Restaurant</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep RAW">EP 14</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/meet-us-at-food-plaza-episode-9.html" class="img" title="Meet Us at Food Plaza EP 9">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Meet Us at Food Plaza" data-original="https://cdn.videokvid.com/cover/meet-us-at-food-plaza.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/meet-us-at-food-plaza-episode-9.html'">Meet Us at Food Plaza</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep RAW">EP 9</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/mister-trot-episode-5.html" class="img" title="Mister Trot EP 5">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Mister Trot" data-original="https://cdn.videokvid.com/cover/mister-trot.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/mister-trot-episode-5.html'">Mister Trot</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep RAW">EP 5</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/morning-forum-episode-8559.html" class="img" title="morning forum EP 8559">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="morning forum" data-original="https://cdn.videokvid.com/20150604073029-morning forum.jpg" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/morning-forum-episode-8559.html'">morning forum</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep RAW">EP 8559</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/soo-mis-side-dishes-episode-86.html" class="img" title="Soo-mi's Side Dishes EP 86">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Soo-mi&amp;#039;s Side Dishes" data-original="https://cdn.videokvid.com/cover/soo-mis-side-dishes.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/soo-mis-side-dishes-episode-86.html'">Soo-mi's Side Dishes</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep RAW">EP 86</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/housekeeping-men-season-2-episode-138.html" class="img" title="Housekeeping Men Season 2 EP 138">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Housekeeping Men Season 2" data-original="https://cdn.videokvid.com/cover/housekeeping-men-season-2.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/housekeeping-men-season-2-episode-138.html'">Housekeeping Men Season 2</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep RAW">EP 138</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/live-info-show-2-episode-1196.html" class="img" title="live info show 2 EP 1196">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="live info show 2" data-original="https://cdn.videokvid.com/20150313030336-live info show 2.jpg" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/live-info-show-2-episode-1196.html'">live info show 2</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep RAW">EP 1196</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/mr-house-husband-episode-147.html" class="img" title="Mr. House Husband EP 147">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Mr. House Husband" data-original="https://cdn.videokvid.com/cover/mr-house-husband.jpg" />				<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/mr-house-husband-episode-147.html'">Mr. House Husband</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep SUB">EP 147</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/because-i-want-to-talk-episode-9.html" class="img" title="Because I Want to Talk EP 9">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Because I Want to Talk" data-original="https://cdn.videokvid.com/cover/because-i-want-to-talk.png" />				<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/because-i-want-to-talk-episode-9.html'">Because I Want to Talk</h3>
                                    <span class="time">1 day ago</span>
                                    <span class="ep SUB">EP 9</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/happy-together-s3-episode-630.html" class="img" title="Happy Together S3 EP 630">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Happy Together S3" data-original="https://cdn.videokvid.com/20141208041208-Happy Together S3.jpg" />				<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/happy-together-s3-episode-630.html'">Happy Together S3</h3>
                                    <span class="time">2 days ago</span>
                                    <span class="ep SUB">EP 630</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/body-god-episode-264.html" class="img" title="Body God EP 264">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Body God" data-original="https://cdn.videokvid.com/cover/body-god.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/body-god-episode-264.html'">Body God</h3>
                                    <span class="time">2 days ago</span>
                                    <span class="ep RAW">EP 264</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/broadcasting-on-your-side-episode-12.html" class="img" title="Broadcasting on Your Side EP 12">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Broadcasting on Your Side" data-original="https://cdn.videokvid.com/cover/broadcasting-on-your-side.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/broadcasting-on-your-side-episode-12.html'">Broadcasting on Your Side</h3>
                                    <span class="time">2 days ago</span>
                                    <span class="ep RAW">EP 12</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/burning-youth-episode-238.html" class="img" title="Burning Youth EP 238">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Burning Youth" data-original="https://cdn.videokvid.com/20160120102637-burning-youth.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/burning-youth-episode-238.html'">Burning Youth</h3>
                                    <span class="time">2 days ago</span>
                                    <span class="ep RAW">EP 238</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/charles-the-next-door-neighbor-episode-223.html" class="img" title="Charles the Next Door Neighbor  EP 223">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Charles the Next Door Neighbor " data-original="https://cdn.videokvid.com/cover/charles-the-next-door-neighbor-.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/charles-the-next-door-neighbor-episode-223.html'">Charles the Next Door Neighbor </h3>
                                    <span class="time">2 days ago</span>
                                    <span class="ep RAW">EP 223</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/idol-room-episode-86.html" class="img" title="Idol Room EP 86">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Idol Room" data-original="https://cdn.videokvid.com/cover/idol-room.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/idol-room-episode-86.html'">Idol Room</h3>
                                    <span class="time">2 days ago</span>
                                    <span class="ep RAW">EP 86</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/same-bed-different-dreams-season-2-episode-131.html" class="img" title="Same Bed, Different Dreams Season 2 EP 131">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Same Bed, Different Dreams Season 2" data-original="https://cdn.videokvid.com/cover/same-bed-different-dreams-season-2.png" />				<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/same-bed-different-dreams-season-2-episode-131.html'">Same Bed, Different Dreams Season 2</h3>
                                    <span class="time">2020-02-04 04:50:22</span>
                                    <span class="ep SUB">EP 131</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/thrifters-guide-to-luxurious-travels-episode-110.html" class="img" title="Thrifter's Guide to Luxurious Travels EP 110">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Thrifter&amp;#039;s Guide to Luxurious Travels" data-original="https://cdn.videokvid.com/cover/thrifters-guide-to-luxurious-travels.png" />				<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/thrifters-guide-to-luxurious-travels-episode-110.html'">Thrifter's Guide to Luxurious Travels</h3>
                                    <span class="time">2020-02-04 04:45:59</span>
                                    <span class="ep SUB">EP 110</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/men-in-black-box-episode-278.html" class="img" title="Men in Black Box EP 278">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Men in Black Box" data-original="https://cdn.videokvid.com/cover/men-in-black-box.jpg" />				<span class="type SUB">SUB</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/men-in-black-box-episode-278.html'">Men in Black Box</h3>
                                    <span class="time">2020-02-04 00:04:02</span>
                                    <span class="ep SUB">EP 278</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/every-question-episode-4504.html" class="img" title="Every Question EP 4504">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Every Question" data-original="https://cdn.videokvid.com/20150815034517-Every Question.jpg" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/every-question-episode-4504.html'">Every Question</h3>
                                    <span class="time">2020-02-03 19:56:14</span>
                                    <span class="ep RAW">EP 4504</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/fly-shoot-new-beginning-episode-4.html" class="img" title="Fly Shoot - New Beginning EP 4">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Fly Shoot - New Beginning" data-original="https://cdn.videokvid.com/cover/fly-shoot-new-beginning.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/fly-shoot-new-beginning-episode-4.html'">Fly Shoot - New Beginning</h3>
                                    <span class="time">2020-02-03 19:54:56</span>
                                    <span class="ep RAW">EP 4</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/friendly-variety-show-episode-4.html" class="img" title="Friendly Variety Show EP 4">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Friendly Variety Show" data-original="https://cdn.videokvid.com/cover/friendly-variety-show.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/friendly-variety-show-episode-4.html'">Friendly Variety Show</h3>
                                    <span class="time">2020-02-03 19:54:38</span>
                                    <span class="ep RAW">EP 4</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/go-all-out-show-episode-9.html" class="img" title="Go All Out Show EP 9">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Go All Out Show" data-original="https://cdn.videokvid.com/cover/go-all-out-show.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/go-all-out-show-episode-9.html'">Go All Out Show</h3>
                                    <span class="time">2020-02-03 19:54:17</span>
                                    <span class="ep RAW">EP 9</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/hometown-report-episode-6959.html" class="img" title="Hometown Report EP 6959">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Hometown Report" data-original="https://cdn.videokvid.com/20150724075619-Hometown Report.jpg" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/hometown-report-episode-6959.html'">Hometown Report</h3>
                                    <span class="time">2020-02-03 19:53:45</span>
                                    <span class="ep RAW">EP 6959</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www1.watchasian.to/jung-hae-ins-walking-report-episode-9.html" class="img" title="Jung Hae In’s Walking Report EP 9">
                                    <img src="https://www1.watchasian.to/images/background.jpg" class="lazy" alt="Jung Hae In’s Walking Report" data-original="https://cdn.videokvid.com/cover/jung-hae-ins-walking-report.png" />				<span class="type RAW">RAW</span>
                                    <h3 class="title" onclick="window.location = 'https://www1.watchasian.to/jung-hae-ins-walking-report-episode-9.html'">Jung Hae In’s Walking Report</h3>
                                    <span class="time">2020-02-03 19:52:57</span>
                                    <span class="ep RAW">EP 9</span>
                                </a>
                            </li>
                        </ul>
                        <div class="view-more">
                            <a href="https://www1.watchasian.to/recently-added-kshow" title="Recently Added Kshow">View more</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="block-tab">
                <ul class="tab">
                    <li data-tab="left-tab-4" class="selected">Most Popular Series</li>
                </ul>
                <div class="block tab-container list-popular">
                    <div class="tab-content left-tab-4 selected">
                        <ul>
                            <li><a href="https://www1.watchasian.to/drama-detail/my-holo-love" title="My Holo Love">My Holo Love</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/the-cursed-2020" title="The Cursed (2020)">The Cursed (2020)</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/tel-me-what-you-saw" title="Tell Me What You Saw">Tell Me What You Saw</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/itaewon-class" title="Itaewon Class">Itaewon Class</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/forest-2020" title="Forest (2020)">Forest (2020)</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/romantic-doctor-teacher-kim-2" title="Romantic Doctor, Teacher Kim 2">Romantic Doctor, Teacher Kim 2</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/the-game-towards-midnight" title="The Game: Towards Zero">The Game: Towards Zero</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/blue-moon" title="XX (2020)">XX (2020)</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/i-ll-find-you-on-a-beautiful-day" title="I'll Find You on a Beautiful Day">I'll Find You on a Beautiful Day</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/touch-2020" title="Touch (2020)">Touch (2020)</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/black-dog" title="Black Dog">Black Dog</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/money-game" title="Money Game">Money Game</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/emergency-lands-of-love" title="Crash Landing on You">Crash Landing on You</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/war-of-prosecutors" title="Diary of a Prosecutor">Diary of a Prosecutor</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/stove-league" title="Stove League">Stove League</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/9-9-billion-women" title="Woman of 9.9 Billion">Woman of 9.9 Billion</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/people-with-flaws" title="Love With Flaws">Love With Flaws</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/chocolate-2019" title="Chocolate (2019)">Chocolate (2019)</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/psychopath-diary" title="Psychopath Diary">Psychopath Diary</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/chief-of-staff-2" title="Chief of Staff 2">Chief of Staff 2</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/elegant-mother-and-daughter" title="Gracious Revenge">Gracious Revenge</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/vip" title="V.I.P">V.I.P</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/down-the-flower-path" title="Down the Flower Path">Down the Flower Path</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/catch-yoo-ryung" title="Catch the Ghost">Catch the Ghost</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/everyone-s-lies" title="The Lies Within">The Lies Within</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/my-country" title="My Country: The New Age">My Country: The New Age</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/july-found-by-chance" title="Extraordinary You">Extraordinary You</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/mung-bean-chronicles" title="The Tale of Nokdu">The Tale of Nokdu</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/love-is-beautiful-life-is-wonderful" title="Beautiful Love, Wonderful Life">Beautiful Love, Wonderful Life</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/melting-me" title="Melting Me Softly">Melting Me Softly</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/cheap-cheonlima-mart" title="Pegasus Market">Pegasus Market</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/miss-lee" title="Miss Lee">Miss Lee</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/vagabond" title="Vagabond">Vagabond</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/the-defender-human-rights" title="The Running Mates: Human Rights">The Running Mates: Human Rights</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/secret-boutique" title="Secret Boutique">Secret Boutique</a></li>
                            <li><a href="https://www1.watchasian.to/drama-detail/when-camellia-blooms" title="When The Camellia Blooms">When The Camellia Blooms</a></li>
                        </ul>
                        <div class="view-more">
                            <a href="https://www1.watchasian.to/most-popular-drama" title="Popular Drama">View more</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-right">
            <div class="block fanpage">
                <div>Follow us on</div>
                <div>
                    <a href="https://dramacool.info/drama-news" target="_blank">News</a>
                    <a href="https://dramacool.info/upcoming-drama.html" target="_blank">Upcoming</a>
                </div>
            </div>
            <div class="block-tab">
                <ul class="tab">
                    <li data-tab="right-tab-1" class="selected">Ads</li>
                    <li data-tab="right-tab-2" >Coming Eps</li>
                    <li data-tab="right-tab-3">Ongoing</li>
                </ul>
                <div class="block tab-container list-right">
                    <div class="tab-content right-tab-1 selected">
                        <h4 class="content-right-title"><i class="fa fa-clock-o"></i> Ads</h4>

                        <div class="ads_place_160">
                            <!-- Composite Start -->
                            <div id="M429792ScriptRootC735151">
                                <div id="M429792PreloadC735151">
                                    Loading...    </div>
                                <script>
                                    (function(){
                                        var D=new Date(),d=document,b='body',ce='createElement',ac='appendChild',st='style',ds='display',n='none',gi='getElementById',lp=d.location.protocol,wp=lp.indexOf('http')==0?lp:'https:';
                                        var i=d[ce]('iframe');i[st][ds]=n;d[gi]("M429792ScriptRootC735151")[ac](i);try{var iw=i.contentWindow.document;iw.open();iw.writeln("<ht"+"ml><bo"+"dy></bo"+"dy></ht"+"ml>");iw.close();var c=iw[b];}
                                        catch(e){var iw=d;var c=d[gi]("M429792ScriptRootC735151");}var dv=iw[ce]('div');dv.id="MG_ID";dv[st][ds]=n;dv.innerHTML=735151;c[ac](dv);
                                        var s=iw[ce]('script');s.async='async';s.defer='defer';s.charset='utf-8';s.src=wp+"//jsc.mgid.com/a/d/adxpub.watchasian.co.735151.js?t="+D.getUTCFullYear()+D.getUTCMonth()+D.getUTCDate()+D.getUTCHours();c[ac](s);})();
                                </script>
                            </div>
                            <!-- Composite End -->
                        </div>
                    </div>

                    <div class="tab-content right-tab-2">
                        <h4 class="content-right-title"><i class="fa fa-clock-o"></i> Coming Episode</h4>
                        <ul>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/christmas-rose-episode-2.html">Christmas Rose Episode 2</a>
                                </h3>
                                <span class="time">Delayed</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/xx-2020-episode-3.html">XX (2020) Episode 3</a>
                                </h3>
                                <span class="time">Delayed</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/stove-league-episode-14.html">Stove League Episode 14</a>
                                </h3>
                                <span class="time">About 11 minutes</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/xx-2020-episode-4.html">XX (2020) Episode 4</a>
                                </h3>
                                <span class="time">About 3 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/touch-2020-episode-11.html">Touch (2020) Episode 11</a>
                                </h3>
                                <span class="time">About 3 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/itaewon-class-episode-3.html">Itaewon Class Episode 3</a>
                                </h3>
                                <span class="time">About 3 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/three-lives-three-worlds-the-pillow-book-episode-19.html">Three Lives, Three Worlds: The Pillow Book Episode 19</a>
                                </h3>
                                <span class="time">About 16 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/three-lives-three-worlds-the-pillow-book-episode-20.html">Three Lives, Three Worlds: The Pillow Book Episode 20</a>
                                </h3>
                                <span class="time">About 16 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/three-lives-three-worlds-the-pillow-book-episode-21.html">Three Lives, Three Worlds: The Pillow Book Episode 21</a>
                                </h3>
                                <span class="time">About 17 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/three-lives-three-worlds-the-pillow-book-episode-22.html">Three Lives, Three Worlds: The Pillow Book Episode 22</a>
                                </h3>
                                <span class="time">About 17 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/three-lives-three-worlds-the-pillow-book-episode-23.html">Three Lives, Three Worlds: The Pillow Book Episode 23</a>
                                </h3>
                                <span class="time">About 17 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/three-lives-three-worlds-the-pillow-book-episode-24.html">Three Lives, Three Worlds: The Pillow Book Episode 24</a>
                                </h3>
                                <span class="time">About 17 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/three-lives-three-worlds-the-pillow-book-episode-25.html">Three Lives, Three Worlds: The Pillow Book Episode 25</a>
                                </h3>
                                <span class="time">About 17 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/three-lives-three-worlds-the-pillow-book-episode-26.html">Three Lives, Three Worlds: The Pillow Book Episode 26</a>
                                </h3>
                                <span class="time">About 17 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/three-lives-three-worlds-the-pillow-book-episode-27.html">Three Lives, Three Worlds: The Pillow Book Episode 27</a>
                                </h3>
                                <span class="time">About 17 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/three-lives-three-worlds-the-pillow-book-episode-28.html">Three Lives, Three Worlds: The Pillow Book Episode 28</a>
                                </h3>
                                <span class="time">About 17 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/three-lives-three-worlds-the-pillow-book-episode-29.html">Three Lives, Three Worlds: The Pillow Book Episode 29</a>
                                </h3>
                                <span class="time">About 17 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/three-lives-three-worlds-the-pillow-book-episode-30.html">Three Lives, Three Worlds: The Pillow Book Episode 30</a>
                                </h3>
                                <span class="time">About 17 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/amor-parti-episode-13.html">Amor Parti Episode 13</a>
                                </h3>
                                <span class="time">About 18 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/myubu-episode-11.html">Myubu Episode 11</a>
                                </h3>
                                <span class="time">About 20 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/beautiful-love-wonderful-life-episode-37.html">Beautiful Love, Wonderful Life Episode 37</a>
                                </h3>
                                <span class="time">About 24 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/the-depths-of-the-shambhala-episode-5.html">The Depths of the Shambhala Episode 5</a>
                                </h3>
                                <span class="time">About 24 hours</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/never-twice-episode-27.html">Never Twice Episode 27</a>
                                </h3>
                                <span class="time">About 1 day</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/crash-landing-on-you-episode-13.html">Crash Landing on You Episode 13</a>
                                </h3>
                                <span class="time">About 1 day</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/the-great-recipe-episode-4.html">The Great Recipe Episode 4</a>
                                </h3>
                                <span class="time">About 1 day</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/stove-league-episode-15.html">Stove League Episode 15</a>
                                </h3>
                                <span class="time">About 1 day</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/macho-house-episode-15.html">Macho House Episode 15</a>
                                </h3>
                                <span class="time">About 1 day</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/the-bunker-season-6-episode-17.html">The Bunker Season 6 Episode 17</a>
                                </h3>
                                <span class="time">About 1 day</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/global-request-show-a-song-for-you-season-4-episode-15.html">Global Request Show- A Song for You Season 4 Episode 15</a>
                                </h3>
                                <span class="time">About 1 day</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/never-twice-episode-28.html">Never Twice Episode 28</a>
                                </h3>
                                <span class="time">About 1 day</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/tell-me-what-you-saw-episode-3.html">Tell Me What You Saw Episode 3</a>
                                </h3>
                                <span class="time">About 1 day</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/queen-love-and-war-episode-15.html">Queen: Love And War Episode 15</a>
                                </h3>
                                <span class="time">About 1 day</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/touch-2020-episode-12.html">Touch (2020) Episode 12</a>
                                </h3>
                                <span class="time">About 1 day</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/itaewon-class-episode-4.html">Itaewon Class Episode 4</a>
                                </h3>
                                <span class="time">About 1 day</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/youth-over-flowers-in-africa-episode-8.html">youth over flowers in africa Episode 8</a>
                                </h3>
                                <span class="time">About 1 day</span>
                            </li>

                            <li>
                                <h3>
                                    <a href="https://www1.watchasian.to/master-of-living-episode-625.html">Master of Living Episode 625</a>
                                </h3>
                                <span class="time">About 1 day</span>
                            </li>
                        </ul>
                        <div class="view-more">
                            <a href="https://www1.watchasian.to/calendar">View more</a>
                        </div>
                    </div>
                    <div class="tab-content right-tab-3">
                        <h4 class="content-right-title"><i class="fa fa-heart"></i> Popular Ongoing</h4>
                        <ul>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/my-holo-love">My Holo Love</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/the-cursed-2020">The Cursed (2020)</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/tel-me-what-you-saw">Tell Me What You Saw</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/itaewon-class">Itaewon Class</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/forest-2020">Forest (2020)</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/romantic-doctor-teacher-kim-2">Romantic Doctor, Teacher Kim 2</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/the-game-towards-midnight">The Game: Towards Zero</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/blue-moon">XX (2020)</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/i-ll-find-you-on-a-beautiful-day">I'll Find You on a Beautiful Day</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/touch-2020">Touch (2020)</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/money-game">Money Game</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/emergency-lands-of-love">Crash Landing on You</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/war-of-prosecutors">Diary of a Prosecutor</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/stove-league">Stove League</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/elegant-mother-and-daughter">Gracious Revenge</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/down-the-flower-path">Down the Flower Path</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/love-is-beautiful-life-is-wonderful">Beautiful Love, Wonderful Life</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/no-second-chance">Never Twice</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/wanna-taste">Want a Taste?</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/left-handed-wife">Left-Handed Wife</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/circle-korean-drama">Circle (Korean Drama)</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/perfect-wife-korean-drama">Perfect Wife (Korean Drama)</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/romantic-doctor-teacher-kim">Romantic Doctor, Teacher Kim</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/running-man">Running Man</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/the-return-of-superman">The Return of Superman</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/king-of-mask-singer">King of Mask Singer</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/love-cuisine">Love Cuisine</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/weekly-idol">Weekly idol</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/happy-together-s3">Happy Together S3</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/abnormal-summit">Abnormal Summit</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/strong-heart">Strong Heart</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/problematic-men">Problematic Men</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/cheongdamdong-111">Cheongdamdong 111</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/brave-family">Brave Family</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/hologram">Hologram</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://www1.watchasian.to/drama-detail/radio-star">Radio Star</a></h3>
                            </li>
                            <li>
                        </ul>
                        <div class="view-more">
                            <a href="https://www1.watchasian.to/popular-ongoing-series.html">View more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <ul>
            <li><a href="https://www1.watchasian.to/privacy" title="Privacy">Privacy</a></li>
            <li><a href="https://www1.watchasian.to/about" title="About Us">About Us</a></li>
            <li><a href="https://www1.watchasian.to/list-genres.html" title="Genres">Genres</a></li>
            <li><a href="https://www1.watchasian.to/disclaimer" title="Disclaimer">Disclaimer</a></li>
            <li><a href="https://www1.watchasian.to/contact-us" title="Contact Us">Contact Us</a></li>
        </ul>
        <button class="btn-btt"><i class="fa fa-arrow-up"></i> TOP</button>
    </footer>
</div>
<div class="mask"></div>
<div id="off_light"></div>
<div style="position:fixed;top:0;left:calc(50% - 485px);display:none;"></div>
<div style="position:fixed;top:0;right:calc(50% - 485px);display:none;"></div>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-101187474-4"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments)};
    gtag('js', new Date());

    gtag('config', 'UA-101187474-4');
</script>




<script type="text/javascript" src="https://www1.watchasian.to/js/jquery.min.js?v=3.5.0"></script>
<script type="text/javascript" src="https://www1.watchasian.to/js/jquery-ui.min.js?v=3.5.0"></script>
<script type="text/javascript" src="https://www1.watchasian.to/plugins/lazyload/lazyload.min.js?v=3.5.0"></script>
<script type="text/javascript" src="https://www1.watchasian.to/js/main.js?v=3.5.0"></script>
<script type="text/javascript" src="https://www1.watchasian.to/js/mobi.js?v=3.5.0"></script>
<script type="text/javascript" src="https://www1.watchasian.to/js/detectmobilebrowser.js"></script>
<script>
    $(window).on('load', function () {
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1203625996334867";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    });


    $("#search-key").autocomplete({
        minLength: 3,
        appendTo: '.form',
        source: function (request, response) {
            $.ajax({
                url: '/search',
                type: 'get',
                dataType: 'json',
                data: {
                    keyword: request.term,
                    type: $("#search-type").val()
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        messages: {
            noResults: '',
            results: function () {
            }
        },
        select: function (event, ui) {
            url = ui.item.url;
            window.location = url;
        }
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li><a>")
            .append("<img src='https://cdn.videokvid.com/" + item.cover + "' /><div class='info'><div>" + item.name + "</div><div>" + item.status + "</div></div><div class='clearfix'></div></a>")
            .appendTo(ul);
    };
</script>


<script type="text/javascript" src="https://www1.watchasian.to/plugins/slideshow/js/greensock.js?v=3.5.0"></script>
<script type="text/javascript" src="https://www1.watchasian.to/plugins/slideshow/js/layerslider.transitions.js?v=3.5.0"></script>
<script type="text/javascript" src="https://www1.watchasian.to/plugins/slideshow/js/layerslider.kreaturamedia.jquery.js?v=3.5.0"></script>

<script>
    $(window).on('load', function () {
        $("#layerslider").layerSlider({
            skin: 'fullwidth',
            autoPlayVideos: false,
            firstLayer: 'random',
            skinsPath: 'plugins/slideshow/skins/'
        });
    });
</script>

</body>
</html>
